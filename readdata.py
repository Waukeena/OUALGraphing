import pickle
import socket
import struct
import time
import carbon.client
import os
import sys
from pprint import pprint as pp


alldata = []
limit = 10 * 1024
pattern = '%d/%m/%Y-%H:%M:%S'
CARBON_SERVER = 'localhost'
CARBON_PORT = 2004

carbon = carbon.client.UDPClient
alldata = []
datafiles = []
timestamps = []
limit = 10 * 1024
pattern = '%d/%m/%Y-%H:%M:%S'
# datadir = './data/2020-01'

for datadir in os.scandir('./data'):
    if datadir.is_dir:
        timestamps = []
        print(datadir.path)
        with open(datadir.path + '/tandem.time', 'r') as timefile:
            for timeFileLine in timefile:
                timestamp = timeFileLine.rstrip('\r\n')
                epoch = int(time.mktime(time.strptime(timestamp, pattern)))
                timestamps.append(epoch)

        try:
            for entry in os.scandir(datadir):
                if entry.name.startswith("tandem.") and entry.name != "tandem.time" and entry.is_file:
                    with open(entry.path, 'r') as datafile:
                        e = iter(timestamps)
                        for dateFileLine in datafile:
                            dataEntry = dateFileLine.rstrip('\r\n')
                            alldata.append((entry.name, (next(e), dataEntry)))
                            if len(alldata) >= limit:
                                # pp(alldata)
                                payload = pickle.dumps(alldata, protocol=2)
                                header = struct.pack("!L", len(payload))
                                message = header + payload
                                sock = socket.socket()
                                sock.connect((CARBON_SERVER, CARBON_PORT))
                                sock.sendall(message)
                                sock.close()
                                alldata = []
        except StopIteration:
            pp(('Ran out of timestamps using file ',entry.name))
