# Taken directly from https://pypi.org/project/carbon-client/#description
# Fixed the missing imports, and yet, it doesn't do Jack.  No clue why.

# You should set ENV variables CARBON_HOST and CARBON_NS
# CARBON_HOST might contains multiple destinations (comma separated)
from time import sleep
from carbon.client import stat
from carbon.client.extras import SimpleCounter
from carbon.client.extras import SimpleTimer
from carbon.client.extras import SimpleCollector

CARBON_HOST = "localhost:2003"
CARBON_NS = "carbon-client"

# Will be pended one or two metrics
# carbon_client.counter_ok
# carbon_client.counter_fail - if exception will be raised
# carbon_client is namespace by default.
with SimpleCounter("counter"):
    sleep(1)

# Will be pended one metric
# carbon_client.timer_ok - if exception will be raised
# carbon_client.timer_fail - if exception will be raised
# carbon_client is namespace by default.
with SimpleTimer("timer"):
    sleep(1)

# Will be pended n metric
# carbon_client.collector
# carbon_client is namespace by default.
with SimpleCollector("collector") as collector:
    collector.add(123)
    collector.add(122)
    collector.add(-10)

# all metrics will sent.
stat.send()
